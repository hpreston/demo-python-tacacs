# Development Resources 
The below are a list of documents, links, and help guides used during development of this application. 

## TAC_PLUS
* https://github.com/hpreston/rpi-networklab/blob/main/tacacs-server.md
* http://manpages.ubuntu.com/manpages/bionic/man8/tac_plus.8.html 
* http://manpages.ubuntu.com/manpages/bionic/man5/tac_plus.conf.5.html 
* https://github.com/ansible/tacacs_plus
* https://github.com/hpreston/nso-package-tacacs-auth

## Flask
* https://flask.palletsprojects.com/en/2.0.x/appcontext/
* https://flask.palletsprojects.com/en/2.0.x/testing/
* https://flask.palletsprojects.com/en/2.0.x/quickstart/
* https://flask.palletsprojects.com/en/2.0.x/quickstart/#url-building
* https://github.com/pallets/flask/blob/2.0.3/examples/tutorial/tests/conftest.py
* https://flask.palletsprojects.com/en/2.0.x/tutorial/views/
* https://flask.palletsprojects.com/en/2.0.x/tutorial/ 

## pytest
* https://understandingdata.com/list-of-python-assert-statements-for-unit-tests/

## Bootstrap 
* https://www.w3schools.com/bootstrap4/default.asp 
