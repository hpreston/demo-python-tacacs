output "public-ip" {
  value = module.ec2_instance.public_ip
}

output "public-dns" {
  value = module.ec2_instance.public_dns
}
